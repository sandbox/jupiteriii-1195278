<?php

/**
 * @file
 * Connects with views and identifies the requirements of the plugin.
 */

function role_table_views_plugins() {
	return array(
		'module' => 'role_table',
		'style' => array(
			'role_table' => array(
				'title' => t('Role Table'),
				'help' => t('Displays Roles as collective results across a table.'),
				'handler' => 'role_table_plugin_style_role_table',
				'theme' => 'role_table_view_role_table',
				'uses row plugin' => TRUE,
				'uses fields' => TRUE,
				'uses options' => TRUE,
				'type' => 'normal',
				),
			),
		);
	}
